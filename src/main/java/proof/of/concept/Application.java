package proof.of.concept;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
@SpringBootApplication
public class Application {

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    public static void main(String... args) {
        SpringApplication.run(Application.class, args);
    }
}

package proof.of.concept.model.frontend;

import lombok.Data;

import java.util.Date;

@Data
public class FrontendProfile {

    private String name;
    private Date createdAt;
    private String address;
    private FrontendPoint point;
    private Integer pegiRating;

}

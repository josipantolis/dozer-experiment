package proof.of.concept.model.backend;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Optional;

@Data
public class BackendProfile {

    private String title;
    private LocalDateTime createdDateTime;
    private BackendLocation location;
    private Optional<Integer> pegiRating = Optional.empty();

}

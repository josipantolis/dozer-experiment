package proof.of.concept.model.backend;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BackendLocation {

    private String address;
    private BackendPoint point;

}

package proof.of.concept.mapper.converters;

import org.dozer.CustomConverter;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Component
public class LocalDateTimeConverter implements CustomConverter {

    private final Clock clock;

    @Autowired
    public LocalDateTimeConverter(Clock clock) {
        this.clock = clock;
    }

    @Override
    public Object convert(Object existingDestinationFieldValue, Object sourceFieldValue, Class<?> destinationClass, Class<?> sourceClass) {
        if (sourceFieldValue == null) {
            return existingDestinationFieldValue;
        }

        if (LocalDateTime.class.equals(destinationClass)) {
            if (Date.class.equals(sourceClass)) {
                return mapDateToLocalDateTime((Date) sourceFieldValue);
            }
            if (Instant.class.equals(sourceClass)) {
                return mapInstantToLocalDateTime((Instant) sourceFieldValue);
            }
        }

        if (LocalDateTime.class.equals(sourceClass)) {
            LocalDateTime source = (LocalDateTime) sourceFieldValue;

            if (Date.class.equals(destinationClass)) {
                return mapLocalDateTimeToDate(source);
            }
            if (Instant.class.equals(sourceClass)) {
                return mapLocalDateTimeToInstant(source);
            }
        }

        throw new MappingException("Either destination or source type should be " + LocalDateTime.class.getName());
    }

    private LocalDateTime mapDateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), clock.getZone());
    }

    private LocalDateTime mapInstantToLocalDateTime(Instant instant) {
        return LocalDateTime.ofInstant(instant, clock.getZone());
    }

    private Date mapLocalDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(clock.getZone()).toInstant());
    }

    private Instant mapLocalDateTimeToInstant(LocalDateTime localDateTime) {
        return localDateTime.atZone(clock.getZone()).toInstant();
    }
}

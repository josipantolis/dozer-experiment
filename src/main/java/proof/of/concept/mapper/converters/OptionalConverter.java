package proof.of.concept.mapper.converters;

import org.dozer.CustomConverter;
import org.dozer.MappingException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OptionalConverter implements CustomConverter {
    @Override
    public Object convert(Object existingDestinationFieldValue, Object sourceFieldValue, Class<?> destinationClass, Class<?> sourceClass) {
        if (Optional.class.equals(sourceClass)) {
            if (sourceFieldValue == null) {
                return existingDestinationFieldValue;
            }
            Optional optional = (Optional) sourceFieldValue;
            return optional.orElse(null);
        }

        if (Optional.class.equals(destinationClass)) {
            return Optional.ofNullable(sourceFieldValue);
        }

        throw new MappingException("Either destination or source type should be " + Optional.class.getName());
    }
}

package proof.of.concept.mapper;

import org.dozer.DozerEventListener;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.FieldsMappingOption;
import org.dozer.loader.api.FieldsMappingOptions;
import org.springframework.beans.factory.annotation.Autowired;
import proof.of.concept.mapper.converters.LocalDateTimeConverter;
import proof.of.concept.mapper.converters.OptionalConverter;

import javax.annotation.PostConstruct;

public abstract class MapperPlugin {

    @Autowired
    private ExtendableMapper mapper;

    @PostConstruct
    private void registerSelf() {
        mapper.addMapper(this);
    }

    public abstract BeanMappingBuilder getMappingBuilder();

    public DozerEventListener getEventListener() {
        return null;
    }

    protected FieldsMappingOption usingLocalDateTimeConverter() {
        return FieldsMappingOptions.customConverter(LocalDateTimeConverter.class);
    }

    protected FieldsMappingOption usingOptionalConverter() {
        return FieldsMappingOptions.customConverter(OptionalConverter.class);
    }
}

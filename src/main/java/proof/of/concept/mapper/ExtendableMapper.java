package proof.of.concept.mapper;

public interface ExtendableMapper {

    void addMapper(MapperPlugin mapperPlugin);
}

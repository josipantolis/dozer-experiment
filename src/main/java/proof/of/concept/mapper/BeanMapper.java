package proof.of.concept.mapper;

import java.util.List;
import java.util.Set;

public interface BeanMapper {

    <S, D> D map(S source, Class<D> destinationClass);

    <S, D> List<D> map(List<S> source, Class<D> destinationClass);

    <S, D> Set<D> map(Set<S> source, Class<D> destinationClass);

}

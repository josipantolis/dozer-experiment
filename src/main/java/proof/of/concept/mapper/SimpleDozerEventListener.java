package proof.of.concept.mapper;

import org.dozer.DozerEventListener;
import org.dozer.event.DozerEvent;

import java.util.function.Consumer;

public class SimpleDozerEventListener implements DozerEventListener {

    public static SimpleDozerEventListener mappingStarted(Consumer<DozerEvent> mappingStartedConsumer) {
        return new SimpleDozerEventListener(){
            @Override
            public void mappingStarted(DozerEvent event) {
                mappingStartedConsumer.accept(event);
            }
        };
    }

    public static SimpleDozerEventListener preWritingDestinationValue(Consumer<DozerEvent> preWritingConsumer) {
        return new SimpleDozerEventListener(){
            @Override
            public void preWritingDestinationValue(DozerEvent event) {
                preWritingConsumer.accept(event);
            }
        };
    }

    public static SimpleDozerEventListener postWritingDestinationValue(Consumer<DozerEvent> postWritingConsumer) {
        return new SimpleDozerEventListener(){
            @Override
            public void postWritingDestinationValue(DozerEvent event) {
                postWritingConsumer.accept(event);
            }
        };
    }

    public static SimpleDozerEventListener mappingFinished(Consumer<DozerEvent> mappingFinishedConsumer) {
        return new SimpleDozerEventListener(){
            @Override
            public void mappingFinished(DozerEvent event) {
                mappingFinishedConsumer.accept(event);
            }
        };
    }

    @Override
    public void mappingStarted(DozerEvent event) {}
    @Override
    public void preWritingDestinationValue(DozerEvent event) {}
    @Override
    public void postWritingDestinationValue(DozerEvent event) {}
    @Override
    public void mappingFinished(DozerEvent event) {}
}

package proof.of.concept.mapper;

import org.dozer.CustomConverter;
import org.dozer.DozerBeanMapper;
import org.dozer.DozerEventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Component
public class ExtendableBeanMapper implements BeanMapper, ExtendableMapper {

    private DozerBeanMapper dozerMapper;
    private List<DozerEventListener> eventListeners;

    @Autowired
    public ExtendableBeanMapper(List<CustomConverter> customConverters) {
        eventListeners = new LinkedList<>();
        dozerMapper = new DozerBeanMapper();
        dozerMapper.setCustomConverters(customConverters);
    }

    @Override
    public void addMapper(MapperPlugin mapper) {
        Optional.ofNullable(mapper.getMappingBuilder())
                .ifPresent(dozerMapper::addMapping);
        Optional.ofNullable(mapper.getEventListener())
                .ifPresent(listener -> {
                    eventListeners.add(listener);
                    dozerMapper.setEventListeners(eventListeners);
                });
    }

    @Override
    public <S, D> D map(S source, Class<D> destinationClass) {
        return Optional.ofNullable(source)
                .map(s -> dozerMapper.map(s, destinationClass))
                .orElse(null);
    }

    @Override
    public <S, D> List<D> map(List<S> source, Class<D> destinationClass) {
        return Optional.ofNullable(source)
                .map(s -> s.stream()
                        .map(e -> map(e, destinationClass))
                        .collect(toList()))
                .orElse(null);
    }

    @Override
    public <S, D> Set<D> map(Set<S> source, Class<D> destinationClass) {
        return Optional.ofNullable(source)
                .map(s -> s.stream()
                        .map(e -> map(e, destinationClass))
                        .collect(toSet()))
                .orElse(null);
    }
}

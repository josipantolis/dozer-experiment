package proof.of.concept;

import org.dozer.DozerEventListener;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.stereotype.Component;
import proof.of.concept.mapper.MapperPlugin;
import proof.of.concept.mapper.SimpleDozerEventListener;
import proof.of.concept.model.backend.BackendProfile;
import proof.of.concept.model.frontend.FrontendProfile;

@Component
public class ProfileMapper extends MapperPlugin {

    @Override
    public BeanMappingBuilder getMappingBuilder() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(BackendProfile.class, FrontendProfile.class)
                        .fields("title", "name")
                        .fields("createdDateTime", "createdAt", usingLocalDateTimeConverter())
                        .fields("location.address", "address")
                        .fields("location.point.lat", "point.latitude")
                        .fields("location.point.lng", "point.longitude")
                        .fields("pegiRating", "pegiRating", usingOptionalConverter());
            }
        };
    }

    @Override
    public DozerEventListener getEventListener() {
        return SimpleDozerEventListener.mappingFinished(event -> {
            Object source = event.getSourceObject();
            Object destination = event.getDestinationObject();

            if (source instanceof BackendProfile) {
                BackendProfile backendProfile = (BackendProfile) source;
                if (backendProfile.getLocation() == null) {
                    ((FrontendProfile) destination).setPoint(null);
                }
            }

            if (source instanceof FrontendProfile) {
                FrontendProfile frontendProfile = (FrontendProfile) source;
                BackendProfile backendProfile = (BackendProfile) destination;
                if (frontendProfile.getPoint() == null) {
                    backendProfile.getLocation().setPoint(null);
                    if (frontendProfile.getAddress() == null) {
                        backendProfile.setLocation(null);
                    }
                }
            }
        });
    }
}

package proof.of.concept;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import proof.of.concept.mapper.BeanMapper;
import proof.of.concept.model.backend.BackendLocation;
import proof.of.concept.model.backend.BackendPoint;
import proof.of.concept.model.backend.BackendProfile;
import proof.of.concept.model.frontend.FrontendPoint;
import proof.of.concept.model.frontend.FrontendProfile;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.BDDAssertions.then;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ProfileMapperTest {

    @Autowired
    private BeanMapper mapper;

    @Test
    public void shouldMapTitle() {
        String givenTitle = "Mocked title";
        BackendProfile givenProfile = new BackendProfile();
        givenProfile.setTitle(givenTitle);

        FrontendProfile actual = mapper.map(givenProfile, FrontendProfile.class);

        FrontendProfile expected = new FrontendProfile();
        expected.setName(givenTitle);
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapName() {
        String givenName = "Mocked name";
        FrontendProfile givenProfile = new FrontendProfile();
        givenProfile.setName(givenName);

        BackendProfile actual = mapper.map(givenProfile, BackendProfile.class);

        BackendProfile expected = new BackendProfile();
        expected.setTitle(givenName);
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapCreatedDateTime() {
        LocalDateTime givenCreatedDateTime = LocalDateTime.of(2015, 7, 22, 22, 39);
        Date givenCreatedAt = Date.from(givenCreatedDateTime.atZone(ZoneId.systemDefault()).toInstant());
        BackendProfile givenProfile = new BackendProfile();
        givenProfile.setCreatedDateTime(givenCreatedDateTime);

        FrontendProfile actual = mapper.map(givenProfile, FrontendProfile.class);

        FrontendProfile expected = new FrontendProfile();
        expected.setCreatedAt(givenCreatedAt);
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapCreatedAt() {
        LocalDateTime givenCreatedDateTime = LocalDateTime.of(2015, 7, 22, 22, 39);
        Date givenCreatedAt = Date.from(givenCreatedDateTime.atZone(ZoneId.systemDefault()).toInstant());
        FrontendProfile givenProfile = new FrontendProfile();
        givenProfile.setCreatedAt(givenCreatedAt);

        BackendProfile actual = mapper.map(givenProfile, BackendProfile.class);

        BackendProfile expected = new BackendProfile();
        expected.setCreatedDateTime(givenCreatedDateTime);
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapLocation() {
        String givenAddress = "Mocked Address";
        double givenLatitude = 1.0;
        double givenLongitude = 2.0;
        BackendLocation givenLocation = new BackendLocation(givenAddress, new BackendPoint(givenLatitude, givenLongitude));
        BackendProfile givenProfile = new BackendProfile();
        givenProfile.setLocation(givenLocation);

        FrontendProfile actual = mapper.map(givenProfile, FrontendProfile.class);

        FrontendProfile expected = new FrontendProfile();
        expected.setAddress(givenAddress);
        expected.setPoint(new FrontendPoint(givenLatitude, givenLongitude));
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapAddress() {
        String givenAddress = "Mocked Address";
        FrontendProfile givenProfile = new FrontendProfile();
        givenProfile.setAddress(givenAddress);

        BackendProfile actual = mapper.map(givenProfile, BackendProfile.class);

        BackendProfile expected = new BackendProfile();
        expected.setLocation(new BackendLocation(givenAddress, null));
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapPoint() {
        Double givenLatitude = 1.0;
        Double givenLongitude = null;
        FrontendProfile givenProfile = new FrontendProfile();
        givenProfile.setPoint(new FrontendPoint(givenLatitude, givenLongitude));

        BackendProfile actual = mapper.map(givenProfile, BackendProfile.class);

        BackendProfile expected = new BackendProfile();
        expected.setLocation(new BackendLocation(null, new BackendPoint(givenLatitude, 0.0)));
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapOptionalPegiRating() {
        Integer givenPegiRating = 18;
        BackendProfile givenProfile = new BackendProfile();
        givenProfile.setPegiRating(Optional.of(givenPegiRating));

        FrontendProfile actual = mapper.map(givenProfile, FrontendProfile.class);

        FrontendProfile expected = new FrontendProfile();
        expected.setPegiRating(givenPegiRating);
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldMapPegiRating() {
        Integer givenPegiRating = 18;
        FrontendProfile givenProfile = new FrontendProfile();
        givenProfile.setPegiRating(givenPegiRating);

        BackendProfile actual = mapper.map(givenProfile, BackendProfile.class);

        BackendProfile expected = new BackendProfile();
        expected.setPegiRating(Optional.of(givenPegiRating));
        then(actual).isEqualTo(expected);
    }
}
